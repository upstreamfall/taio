﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphSimilarity
{
    class ObjectRepository
    {
        Graph[] _graphs;

        public ObjectRepository(int numberOfGraphs)
        {
            _graphs = new Graph[numberOfGraphs];
            for (int i = 0; i < numberOfGraphs; ++i)
            {
                _graphs[i] = new Graph();
            }
        }

        public Graph this[int key]
        {
            get
            {
                return _graphs[key];
            }
            set
            {
                _graphs[key] = value;
            }
        }


        public Graph GetGraph(int choosenGraph)
        {
            return _graphs[choosenGraph];
        }

        public void SetGraph(int choosenGraph, Graph newGraph)
        {
            _graphs[choosenGraph] = new Graph(newGraph);
        }
    }
}
