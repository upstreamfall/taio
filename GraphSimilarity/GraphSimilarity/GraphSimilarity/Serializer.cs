﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphSimilarity
{
    class Serializer
    {
        internal void WriteToFile(string name, Graph graph)
        {
            File.WriteAllText(name, graph.ToString());
        }

        internal Graph ReadFromFile(string fileName)
        {
            byte[,] matrix = null;
            try
            {
                string[] lines = System.IO.File.ReadAllLines(fileName);

                int verticesCounter = int.Parse(lines[0]);
                matrix = new byte[verticesCounter, verticesCounter];
                for (int i = 1; i < lines.Count(); ++i)
                {
                    string line = lines[i];
                    for (int j = 0; j < lines.Length - 1; ++j)
                        matrix[i - 1, j] = (byte)(line[j] == '0' ? 0 : 1);
                }
            }
            catch (IOException)
            {
            }

            return new Graph(matrix);
        }
    }
}
