﻿namespace GraphSimilarity
{
    partial class ApplicationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.algorithmChoiceGroupBox = new System.Windows.Forms.GroupBox();
            this.countButton = new System.Windows.Forms.Button();
            this.approximateAlgorithmRadioButton = new System.Windows.Forms.RadioButton();
            this.exactAlgorithmRadioButton = new System.Windows.Forms.RadioButton();
            this.manualAddGroupBox = new System.Windows.Forms.GroupBox();
            this.renumberButton = new System.Windows.Forms.Button();
            this.ComplementaryGraphButton = new System.Windows.Forms.Button();
            this.DeleteEdgeButton = new System.Windows.Forms.Button();
            this.DeleteVertexButton = new System.Windows.Forms.Button();
            this.AddVertexButton = new System.Windows.Forms.Button();
            this.manualDeleteButton = new System.Windows.Forms.Button();
            this.manualGenerateButton = new System.Windows.Forms.Button();
            this.manualVerticesCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.manualVerticesLabel = new System.Windows.Forms.Label();
            this.randomAddGroupBox = new System.Windows.Forms.GroupBox();
            this.randomEdgesDensityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.completeGraphCheckBox = new System.Windows.Forms.CheckBox();
            this.randomGenerateButton = new System.Windows.Forms.Button();
            this.randomEdgesCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.randomEdgesLabel = new System.Windows.Forms.Label();
            this.randomVerticesCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.randomVerticesLabel = new System.Windows.Forms.Label();
            this.fileGroupBox = new System.Windows.Forms.GroupBox();
            this.openFileButton = new System.Windows.Forms.Button();
            this.saveFileButton = new System.Windows.Forms.Button();
            this.graphChoiceGroupBox = new System.Windows.Forms.GroupBox();
            this.graph2RadioButton = new System.Windows.Forms.RadioButton();
            this.graph1RadioButton = new System.Windows.Forms.RadioButton();
            this.resultLabel = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.graphContainer = new System.Windows.Forms.SplitContainer();
            this.pictureBox0 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mousePointTextBox0 = new System.Windows.Forms.TextBox();
            this.mousePointTextBox1 = new System.Windows.Forms.TextBox();
            this.numberOfDrawingVerticesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.numberOfDrawingVerticesLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.timeTextBox = new System.Windows.Forms.TextBox();
            this.timeLabel = new System.Windows.Forms.Label();
            this.graph1NameLabel = new System.Windows.Forms.Label();
            this.graph2NameLabel = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.fileName1Label = new System.Windows.Forms.Label();
            this.fileName2Label = new System.Windows.Forms.Label();
            this.algorithmChoiceGroupBox.SuspendLayout();
            this.manualAddGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manualVerticesCountNumericUpDown)).BeginInit();
            this.randomAddGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.randomEdgesDensityNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.randomEdgesCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.randomVerticesCountNumericUpDown)).BeginInit();
            this.fileGroupBox.SuspendLayout();
            this.graphChoiceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graphContainer)).BeginInit();
            this.graphContainer.Panel1.SuspendLayout();
            this.graphContainer.Panel2.SuspendLayout();
            this.graphContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfDrawingVerticesNumericUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // algorithmChoiceGroupBox
            // 
            this.algorithmChoiceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.algorithmChoiceGroupBox.Controls.Add(this.countButton);
            this.algorithmChoiceGroupBox.Controls.Add(this.approximateAlgorithmRadioButton);
            this.algorithmChoiceGroupBox.Controls.Add(this.exactAlgorithmRadioButton);
            this.algorithmChoiceGroupBox.Location = new System.Drawing.Point(1196, 12);
            this.algorithmChoiceGroupBox.Name = "algorithmChoiceGroupBox";
            this.algorithmChoiceGroupBox.Size = new System.Drawing.Size(146, 93);
            this.algorithmChoiceGroupBox.TabIndex = 2;
            this.algorithmChoiceGroupBox.TabStop = false;
            this.algorithmChoiceGroupBox.Text = "Wybór algorytmu";
            // 
            // countButton
            // 
            this.countButton.Location = new System.Drawing.Point(6, 64);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(130, 23);
            this.countButton.TabIndex = 2;
            this.countButton.Text = "Oblicz współczynnik";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // approximateAlgorithmRadioButton
            // 
            this.approximateAlgorithmRadioButton.AutoSize = true;
            this.approximateAlgorithmRadioButton.Location = new System.Drawing.Point(6, 41);
            this.approximateAlgorithmRadioButton.Name = "approximateAlgorithmRadioButton";
            this.approximateAlgorithmRadioButton.Size = new System.Drawing.Size(119, 17);
            this.approximateAlgorithmRadioButton.TabIndex = 1;
            this.approximateAlgorithmRadioButton.Text = "Algorytm przybliżony";
            this.approximateAlgorithmRadioButton.UseVisualStyleBackColor = true;
            // 
            // exactAlgorithmRadioButton
            // 
            this.exactAlgorithmRadioButton.AutoSize = true;
            this.exactAlgorithmRadioButton.Checked = true;
            this.exactAlgorithmRadioButton.Location = new System.Drawing.Point(6, 19);
            this.exactAlgorithmRadioButton.Name = "exactAlgorithmRadioButton";
            this.exactAlgorithmRadioButton.Size = new System.Drawing.Size(113, 17);
            this.exactAlgorithmRadioButton.TabIndex = 0;
            this.exactAlgorithmRadioButton.TabStop = true;
            this.exactAlgorithmRadioButton.Text = "Algorytm dokładny";
            this.exactAlgorithmRadioButton.UseVisualStyleBackColor = true;
            // 
            // manualAddGroupBox
            // 
            this.manualAddGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.manualAddGroupBox.Controls.Add(this.renumberButton);
            this.manualAddGroupBox.Controls.Add(this.ComplementaryGraphButton);
            this.manualAddGroupBox.Controls.Add(this.DeleteEdgeButton);
            this.manualAddGroupBox.Controls.Add(this.DeleteVertexButton);
            this.manualAddGroupBox.Controls.Add(this.AddVertexButton);
            this.manualAddGroupBox.Controls.Add(this.manualDeleteButton);
            this.manualAddGroupBox.Controls.Add(this.manualGenerateButton);
            this.manualAddGroupBox.Controls.Add(this.manualVerticesCountNumericUpDown);
            this.manualAddGroupBox.Controls.Add(this.manualVerticesLabel);
            this.manualAddGroupBox.Location = new System.Drawing.Point(1196, 156);
            this.manualAddGroupBox.Name = "manualAddGroupBox";
            this.manualAddGroupBox.Size = new System.Drawing.Size(146, 272);
            this.manualAddGroupBox.TabIndex = 4;
            this.manualAddGroupBox.TabStop = false;
            this.manualAddGroupBox.Text = "Manualne tworzenie grafów";
            // 
            // renumberButton
            // 
            this.renumberButton.Location = new System.Drawing.Point(6, 244);
            this.renumberButton.Name = "renumberButton";
            this.renumberButton.Size = new System.Drawing.Size(130, 23);
            this.renumberButton.TabIndex = 8;
            this.renumberButton.Text = "Przenumerowanie";
            this.renumberButton.UseVisualStyleBackColor = true;
            this.renumberButton.Click += new System.EventHandler(this.renumberButton_Click);
            // 
            // ComplementaryGraphButton
            // 
            this.ComplementaryGraphButton.Location = new System.Drawing.Point(6, 215);
            this.ComplementaryGraphButton.Name = "ComplementaryGraphButton";
            this.ComplementaryGraphButton.Size = new System.Drawing.Size(130, 23);
            this.ComplementaryGraphButton.TabIndex = 7;
            this.ComplementaryGraphButton.Text = "Dopełnienie grafu";
            this.ComplementaryGraphButton.UseVisualStyleBackColor = true;
            this.ComplementaryGraphButton.Click += new System.EventHandler(this.ComplementaryGraphButton_Click);
            // 
            // DeleteEdgeButton
            // 
            this.DeleteEdgeButton.Enabled = false;
            this.DeleteEdgeButton.Location = new System.Drawing.Point(6, 187);
            this.DeleteEdgeButton.Name = "DeleteEdgeButton";
            this.DeleteEdgeButton.Size = new System.Drawing.Size(130, 23);
            this.DeleteEdgeButton.TabIndex = 6;
            this.DeleteEdgeButton.Text = "Usuń krawędź";
            this.DeleteEdgeButton.UseVisualStyleBackColor = true;
            this.DeleteEdgeButton.Click += new System.EventHandler(this.DeleteEdgeButton_Click);
            // 
            // DeleteVertexButton
            // 
            this.DeleteVertexButton.Enabled = false;
            this.DeleteVertexButton.Location = new System.Drawing.Point(6, 158);
            this.DeleteVertexButton.Name = "DeleteVertexButton";
            this.DeleteVertexButton.Size = new System.Drawing.Size(130, 23);
            this.DeleteVertexButton.TabIndex = 5;
            this.DeleteVertexButton.Text = "Usuń wierzchołek";
            this.DeleteVertexButton.UseVisualStyleBackColor = true;
            this.DeleteVertexButton.Click += new System.EventHandler(this.DeleteVertexButton_Click);
            // 
            // AddVertexButton
            // 
            this.AddVertexButton.Location = new System.Drawing.Point(6, 129);
            this.AddVertexButton.Name = "AddVertexButton";
            this.AddVertexButton.Size = new System.Drawing.Size(130, 23);
            this.AddVertexButton.TabIndex = 4;
            this.AddVertexButton.Text = "Dodaj wierzchołek";
            this.AddVertexButton.UseVisualStyleBackColor = true;
            this.AddVertexButton.Click += new System.EventHandler(this.AddVertexButton_Click);
            // 
            // manualDeleteButton
            // 
            this.manualDeleteButton.Location = new System.Drawing.Point(6, 100);
            this.manualDeleteButton.Name = "manualDeleteButton";
            this.manualDeleteButton.Size = new System.Drawing.Size(130, 23);
            this.manualDeleteButton.TabIndex = 3;
            this.manualDeleteButton.Text = "Usuń graf";
            this.manualDeleteButton.UseVisualStyleBackColor = true;
            this.manualDeleteButton.Click += new System.EventHandler(this.manualDeleteButton_Click);
            // 
            // manualGenerateButton
            // 
            this.manualGenerateButton.Location = new System.Drawing.Point(6, 74);
            this.manualGenerateButton.Name = "manualGenerateButton";
            this.manualGenerateButton.Size = new System.Drawing.Size(130, 23);
            this.manualGenerateButton.TabIndex = 2;
            this.manualGenerateButton.Text = "Stwórz graf";
            this.manualGenerateButton.UseVisualStyleBackColor = true;
            this.manualGenerateButton.Click += new System.EventHandler(this.manualGenerateButton_Click);
            // 
            // manualVerticesCountNumericUpDown
            // 
            this.manualVerticesCountNumericUpDown.Location = new System.Drawing.Point(10, 48);
            this.manualVerticesCountNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.manualVerticesCountNumericUpDown.Name = "manualVerticesCountNumericUpDown";
            this.manualVerticesCountNumericUpDown.Size = new System.Drawing.Size(130, 20);
            this.manualVerticesCountNumericUpDown.TabIndex = 1;
            // 
            // manualVerticesLabel
            // 
            this.manualVerticesLabel.AutoSize = true;
            this.manualVerticesLabel.Location = new System.Drawing.Point(7, 31);
            this.manualVerticesLabel.Name = "manualVerticesLabel";
            this.manualVerticesLabel.Size = new System.Drawing.Size(110, 13);
            this.manualVerticesLabel.TabIndex = 0;
            this.manualVerticesLabel.Text = "Liczba wierzchołków:";
            // 
            // randomAddGroupBox
            // 
            this.randomAddGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.randomAddGroupBox.Controls.Add(this.randomEdgesDensityNumericUpDown);
            this.randomAddGroupBox.Controls.Add(this.label1);
            this.randomAddGroupBox.Controls.Add(this.completeGraphCheckBox);
            this.randomAddGroupBox.Controls.Add(this.randomGenerateButton);
            this.randomAddGroupBox.Controls.Add(this.randomEdgesCountNumericUpDown);
            this.randomAddGroupBox.Controls.Add(this.randomEdgesLabel);
            this.randomAddGroupBox.Controls.Add(this.randomVerticesCountNumericUpDown);
            this.randomAddGroupBox.Controls.Add(this.randomVerticesLabel);
            this.randomAddGroupBox.Location = new System.Drawing.Point(1196, 434);
            this.randomAddGroupBox.Name = "randomAddGroupBox";
            this.randomAddGroupBox.Size = new System.Drawing.Size(146, 207);
            this.randomAddGroupBox.TabIndex = 5;
            this.randomAddGroupBox.TabStop = false;
            this.randomAddGroupBox.Text = "Tworzenie losowych grafów";
            // 
            // randomEdgesDensityNumericUpDown
            // 
            this.randomEdgesDensityNumericUpDown.Location = new System.Drawing.Point(10, 129);
            this.randomEdgesDensityNumericUpDown.Name = "randomEdgesDensityNumericUpDown";
            this.randomEdgesDensityNumericUpDown.Size = new System.Drawing.Size(130, 20);
            this.randomEdgesDensityNumericUpDown.TabIndex = 8;
            this.randomEdgesDensityNumericUpDown.ValueChanged += new System.EventHandler(this.randomEdgesDensityNumericUpDown_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Gęstość [%]:";
            // 
            // completeGraphCheckBox
            // 
            this.completeGraphCheckBox.AutoSize = true;
            this.completeGraphCheckBox.Location = new System.Drawing.Point(10, 155);
            this.completeGraphCheckBox.Name = "completeGraphCheckBox";
            this.completeGraphCheckBox.Size = new System.Drawing.Size(76, 17);
            this.completeGraphCheckBox.TabIndex = 6;
            this.completeGraphCheckBox.Text = "Graf pełny";
            this.completeGraphCheckBox.UseVisualStyleBackColor = true;
            this.completeGraphCheckBox.CheckedChanged += new System.EventHandler(this.completeGraphCheckBox_CheckedChanged);
            // 
            // randomGenerateButton
            // 
            this.randomGenerateButton.Location = new System.Drawing.Point(10, 178);
            this.randomGenerateButton.Name = "randomGenerateButton";
            this.randomGenerateButton.Size = new System.Drawing.Size(130, 23);
            this.randomGenerateButton.TabIndex = 3;
            this.randomGenerateButton.Text = "Generuj graf";
            this.randomGenerateButton.UseVisualStyleBackColor = true;
            this.randomGenerateButton.Click += new System.EventHandler(this.randomGenerateButton_Click);
            // 
            // randomEdgesCountNumericUpDown
            // 
            this.randomEdgesCountNumericUpDown.Location = new System.Drawing.Point(10, 89);
            this.randomEdgesCountNumericUpDown.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.randomEdgesCountNumericUpDown.Name = "randomEdgesCountNumericUpDown";
            this.randomEdgesCountNumericUpDown.Size = new System.Drawing.Size(130, 20);
            this.randomEdgesCountNumericUpDown.TabIndex = 5;
            this.randomEdgesCountNumericUpDown.ValueChanged += new System.EventHandler(this.randomEdgesCountNumericUpDown_ValueChanged);
            // 
            // randomEdgesLabel
            // 
            this.randomEdgesLabel.AutoSize = true;
            this.randomEdgesLabel.Location = new System.Drawing.Point(7, 72);
            this.randomEdgesLabel.Name = "randomEdgesLabel";
            this.randomEdgesLabel.Size = new System.Drawing.Size(86, 13);
            this.randomEdgesLabel.TabIndex = 4;
            this.randomEdgesLabel.Text = "Liczba krawędzi:";
            // 
            // randomVerticesCountNumericUpDown
            // 
            this.randomVerticesCountNumericUpDown.Location = new System.Drawing.Point(10, 46);
            this.randomVerticesCountNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.randomVerticesCountNumericUpDown.Name = "randomVerticesCountNumericUpDown";
            this.randomVerticesCountNumericUpDown.Size = new System.Drawing.Size(130, 20);
            this.randomVerticesCountNumericUpDown.TabIndex = 3;
            this.randomVerticesCountNumericUpDown.ValueChanged += new System.EventHandler(this.randomVerticesCountNumericUpDown_ValueChanged);
            // 
            // randomVerticesLabel
            // 
            this.randomVerticesLabel.AutoSize = true;
            this.randomVerticesLabel.Location = new System.Drawing.Point(7, 29);
            this.randomVerticesLabel.Name = "randomVerticesLabel";
            this.randomVerticesLabel.Size = new System.Drawing.Size(110, 13);
            this.randomVerticesLabel.TabIndex = 2;
            this.randomVerticesLabel.Text = "Liczba wierzchołków:";
            // 
            // fileGroupBox
            // 
            this.fileGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fileGroupBox.Controls.Add(this.openFileButton);
            this.fileGroupBox.Controls.Add(this.saveFileButton);
            this.fileGroupBox.Location = new System.Drawing.Point(1196, 645);
            this.fileGroupBox.Name = "fileGroupBox";
            this.fileGroupBox.Size = new System.Drawing.Size(146, 81);
            this.fileGroupBox.TabIndex = 6;
            this.fileGroupBox.TabStop = false;
            this.fileGroupBox.Text = "Zapis/odczyt z pliku";
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(10, 50);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(130, 23);
            this.openFileButton.TabIndex = 1;
            this.openFileButton.Text = "Wczytaj z pliku";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // saveFileButton
            // 
            this.saveFileButton.Location = new System.Drawing.Point(10, 20);
            this.saveFileButton.Name = "saveFileButton";
            this.saveFileButton.Size = new System.Drawing.Size(130, 23);
            this.saveFileButton.TabIndex = 0;
            this.saveFileButton.Text = "Zapisz do pliku";
            this.saveFileButton.UseVisualStyleBackColor = true;
            this.saveFileButton.Click += new System.EventHandler(this.saveFileButton_Click);
            // 
            // graphChoiceGroupBox
            // 
            this.graphChoiceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.graphChoiceGroupBox.Controls.Add(this.graph2RadioButton);
            this.graphChoiceGroupBox.Controls.Add(this.graph1RadioButton);
            this.graphChoiceGroupBox.Location = new System.Drawing.Point(1196, 105);
            this.graphChoiceGroupBox.Name = "graphChoiceGroupBox";
            this.graphChoiceGroupBox.Size = new System.Drawing.Size(146, 45);
            this.graphChoiceGroupBox.TabIndex = 3;
            this.graphChoiceGroupBox.TabStop = false;
            this.graphChoiceGroupBox.Text = "Wybór grafu";
            // 
            // graph2RadioButton
            // 
            this.graph2RadioButton.AutoSize = true;
            this.graph2RadioButton.Location = new System.Drawing.Point(82, 19);
            this.graph2RadioButton.Name = "graph2RadioButton";
            this.graph2RadioButton.Size = new System.Drawing.Size(54, 17);
            this.graph2RadioButton.TabIndex = 1;
            this.graph2RadioButton.Text = "Graf 2";
            this.graph2RadioButton.UseVisualStyleBackColor = true;
            // 
            // graph1RadioButton
            // 
            this.graph1RadioButton.AutoSize = true;
            this.graph1RadioButton.Checked = true;
            this.graph1RadioButton.Location = new System.Drawing.Point(6, 19);
            this.graph1RadioButton.Name = "graph1RadioButton";
            this.graph1RadioButton.Size = new System.Drawing.Size(54, 17);
            this.graph1RadioButton.TabIndex = 0;
            this.graph1RadioButton.TabStop = true;
            this.graph1RadioButton.Text = "Graf 1";
            this.graph1RadioButton.UseVisualStyleBackColor = true;
            this.graph1RadioButton.CheckedChanged += new System.EventHandler(this.graph1RadioButton_CheckedChanged);
            // 
            // resultLabel
            // 
            this.resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new System.Drawing.Point(6, 21);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(147, 13);
            this.resultLabel.TabIndex = 8;
            this.resultLabel.Text = "Współczynnik podobieństwa:";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resultTextBox.Location = new System.Drawing.Point(159, 18);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.Size = new System.Drawing.Size(79, 20);
            this.resultTextBox.TabIndex = 9;
            this.resultTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.FileName = "graf_1.txt";
            this.saveFileDialog.Filter = "Graf (.txt)|*.txt";
            this.saveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_FileOk);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFile";
            // 
            // graphContainer
            // 
            this.graphContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphContainer.IsSplitterFixed = true;
            this.graphContainer.Location = new System.Drawing.Point(12, 31);
            this.graphContainer.Name = "graphContainer";
            // 
            // graphContainer.Panel1
            // 
            this.graphContainer.Panel1.Controls.Add(this.pictureBox0);
            // 
            // graphContainer.Panel2
            // 
            this.graphContainer.Panel2.Controls.Add(this.pictureBox1);
            this.graphContainer.Size = new System.Drawing.Size(1178, 659);
            this.graphContainer.SplitterDistance = 586;
            this.graphContainer.TabIndex = 10;
            // 
            // pictureBox0
            // 
            this.pictureBox0.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox0.BackColor = System.Drawing.Color.Black;
            this.pictureBox0.Location = new System.Drawing.Point(0, 23);
            this.pictureBox0.Name = "pictureBox0";
            this.pictureBox0.Size = new System.Drawing.Size(578, 630);
            this.pictureBox0.TabIndex = 0;
            this.pictureBox0.TabStop = false;
            this.pictureBox0.SizeChanged += new System.EventHandler(this.pictureBox0_SizeChanged);
            this.pictureBox0.DoubleClick += new System.EventHandler(this.pictureBox0_DoubleClick);
            this.pictureBox0.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox0_MouseMove);
            this.pictureBox0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox0_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(0, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(579, 631);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // mousePointTextBox0
            // 
            this.mousePointTextBox0.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mousePointTextBox0.Location = new System.Drawing.Point(1010, 18);
            this.mousePointTextBox0.Name = "mousePointTextBox0";
            this.mousePointTextBox0.Size = new System.Drawing.Size(57, 20);
            this.mousePointTextBox0.TabIndex = 11;
            this.mousePointTextBox0.Visible = false;
            // 
            // mousePointTextBox1
            // 
            this.mousePointTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mousePointTextBox1.Location = new System.Drawing.Point(1132, 18);
            this.mousePointTextBox1.Name = "mousePointTextBox1";
            this.mousePointTextBox1.Size = new System.Drawing.Size(57, 20);
            this.mousePointTextBox1.TabIndex = 12;
            this.mousePointTextBox1.Visible = false;
            // 
            // numberOfDrawingVerticesNumericUpDown
            // 
            this.numberOfDrawingVerticesNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.numberOfDrawingVerticesNumericUpDown.Location = new System.Drawing.Point(787, 19);
            this.numberOfDrawingVerticesNumericUpDown.Name = "numberOfDrawingVerticesNumericUpDown";
            this.numberOfDrawingVerticesNumericUpDown.Size = new System.Drawing.Size(82, 20);
            this.numberOfDrawingVerticesNumericUpDown.TabIndex = 14;
            this.numberOfDrawingVerticesNumericUpDown.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numberOfDrawingVerticesNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numberOfDrawingVerticesLabel
            // 
            this.numberOfDrawingVerticesLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.numberOfDrawingVerticesLabel.AutoSize = true;
            this.numberOfDrawingVerticesLabel.Location = new System.Drawing.Point(600, 21);
            this.numberOfDrawingVerticesLabel.Name = "numberOfDrawingVerticesLabel";
            this.numberOfDrawingVerticesLabel.Size = new System.Drawing.Size(181, 13);
            this.numberOfDrawingVerticesLabel.TabIndex = 15;
            this.numberOfDrawingVerticesLabel.Text = "Liczba wyświetlanych wierzchołków:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.timeTextBox);
            this.groupBox1.Controls.Add(this.timeLabel);
            this.groupBox1.Controls.Add(this.mousePointTextBox1);
            this.groupBox1.Controls.Add(this.resultLabel);
            this.groupBox1.Controls.Add(this.mousePointTextBox0);
            this.groupBox1.Controls.Add(this.numberOfDrawingVerticesLabel);
            this.groupBox1.Controls.Add(this.numberOfDrawingVerticesNumericUpDown);
            this.groupBox1.Controls.Add(this.resultTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 684);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1354, 49);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // timeTextBox
            // 
            this.timeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.timeTextBox.Location = new System.Drawing.Point(358, 18);
            this.timeTextBox.Name = "timeTextBox";
            this.timeTextBox.ReadOnly = true;
            this.timeTextBox.Size = new System.Drawing.Size(168, 20);
            this.timeTextBox.TabIndex = 17;
            this.timeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // timeLabel
            // 
            this.timeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(244, 21);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(105, 13);
            this.timeLabel.TabIndex = 16;
            this.timeLabel.Text = "Czas obliczania (ms):";
            // 
            // graph1NameLabel
            // 
            this.graph1NameLabel.AutoSize = true;
            this.graph1NameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.graph1NameLabel.Location = new System.Drawing.Point(0, 0);
            this.graph1NameLabel.Name = "graph1NameLabel";
            this.graph1NameLabel.Size = new System.Drawing.Size(0, 13);
            this.graph1NameLabel.TabIndex = 17;
            // 
            // graph2NameLabel
            // 
            this.graph2NameLabel.AutoSize = true;
            this.graph2NameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.graph2NameLabel.Location = new System.Drawing.Point(0, 0);
            this.graph2NameLabel.Name = "graph2NameLabel";
            this.graph2NameLabel.Size = new System.Drawing.Size(0, 13);
            this.graph2NameLabel.TabIndex = 18;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(11, 9);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.fileName1Label);
            this.splitContainer1.Panel1.Controls.Add(this.graph1NameLabel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.fileName2Label);
            this.splitContainer1.Panel2.Controls.Add(this.graph2NameLabel);
            this.splitContainer1.Size = new System.Drawing.Size(1178, 39);
            this.splitContainer1.SplitterDistance = 582;
            this.splitContainer1.TabIndex = 19;
            // 
            // fileName1Label
            // 
            this.fileName1Label.AutoSize = true;
            this.fileName1Label.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.fileName1Label.Location = new System.Drawing.Point(0, 26);
            this.fileName1Label.Name = "fileName1Label";
            this.fileName1Label.Size = new System.Drawing.Size(0, 13);
            this.fileName1Label.TabIndex = 18;
            // 
            // fileName2Label
            // 
            this.fileName2Label.AutoSize = true;
            this.fileName2Label.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.fileName2Label.Location = new System.Drawing.Point(0, 26);
            this.fileName2Label.Name = "fileName2Label";
            this.fileName2Label.Size = new System.Drawing.Size(0, 13);
            this.fileName2Label.TabIndex = 19;
            // 
            // ApplicationWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.graphContainer);
            this.Controls.Add(this.graphChoiceGroupBox);
            this.Controls.Add(this.fileGroupBox);
            this.Controls.Add(this.randomAddGroupBox);
            this.Controls.Add(this.manualAddGroupBox);
            this.Controls.Add(this.algorithmChoiceGroupBox);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(1200, 700);
            this.Name = "ApplicationWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.algorithmChoiceGroupBox.ResumeLayout(false);
            this.algorithmChoiceGroupBox.PerformLayout();
            this.manualAddGroupBox.ResumeLayout(false);
            this.manualAddGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manualVerticesCountNumericUpDown)).EndInit();
            this.randomAddGroupBox.ResumeLayout(false);
            this.randomAddGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.randomEdgesDensityNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.randomEdgesCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.randomVerticesCountNumericUpDown)).EndInit();
            this.fileGroupBox.ResumeLayout(false);
            this.graphChoiceGroupBox.ResumeLayout(false);
            this.graphChoiceGroupBox.PerformLayout();
            this.graphContainer.Panel1.ResumeLayout(false);
            this.graphContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graphContainer)).EndInit();
            this.graphContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfDrawingVerticesNumericUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox algorithmChoiceGroupBox;
        private System.Windows.Forms.GroupBox manualAddGroupBox;
        private System.Windows.Forms.GroupBox randomAddGroupBox;
        private System.Windows.Forms.GroupBox fileGroupBox;
        private System.Windows.Forms.GroupBox graphChoiceGroupBox;
        private System.Windows.Forms.RadioButton approximateAlgorithmRadioButton;
        private System.Windows.Forms.RadioButton exactAlgorithmRadioButton;
        private System.Windows.Forms.RadioButton graph2RadioButton;
        private System.Windows.Forms.RadioButton graph1RadioButton;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.NumericUpDown manualVerticesCountNumericUpDown;
        private System.Windows.Forms.Label manualVerticesLabel;
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.Button saveFileButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button manualGenerateButton;
        private System.Windows.Forms.Button randomGenerateButton;
        private System.Windows.Forms.NumericUpDown randomEdgesCountNumericUpDown;
        private System.Windows.Forms.Label randomEdgesLabel;
        private System.Windows.Forms.NumericUpDown randomVerticesCountNumericUpDown;
        private System.Windows.Forms.Label randomVerticesLabel;
        private System.Windows.Forms.Button manualDeleteButton;
        private System.Windows.Forms.CheckBox completeGraphCheckBox;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.SplitContainer graphContainer;
        private System.Windows.Forms.PictureBox pictureBox0;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button DeleteVertexButton;
        private System.Windows.Forms.Button AddVertexButton;
        private System.Windows.Forms.Button DeleteEdgeButton;
        private System.Windows.Forms.TextBox mousePointTextBox0;
        private System.Windows.Forms.TextBox mousePointTextBox1;
        private System.Windows.Forms.NumericUpDown numberOfDrawingVerticesNumericUpDown;
        private System.Windows.Forms.Label numberOfDrawingVerticesLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Button ComplementaryGraphButton;
        private System.Windows.Forms.Button renumberButton;
        private System.Windows.Forms.TextBox timeTextBox;
        private System.Windows.Forms.Label graph1NameLabel;
        private System.Windows.Forms.Label graph2NameLabel;
        private System.Windows.Forms.NumericUpDown randomEdgesDensityNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label fileName1Label;
        private System.Windows.Forms.Label fileName2Label;

    }
}

