﻿using accurateGraphMatchingAlgorithm;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace GraphSimilarity
{
    public partial class ApplicationWindow : Form
    {
        private int _width = 800;
        private int _height = 600;

        private int _choosenGraph;
        private ObjectRepository _repository;
        private Serializer _serializer;

        private AccurateAlgorithm _accurateAlgorithm;
        private ApproximationAlgorithm _approximateAlgorithm;

        private DrawingManager _drawingManager;

        public ApplicationWindow()
        {
            _repository = new ObjectRepository(2);
            _serializer = new Serializer();
            _choosenGraph = 0;
            _accurateAlgorithm = new AccurateAlgorithm();
            _approximateAlgorithm = new ApproximationAlgorithm();
            _drawingManager = new DrawingManager(_width, _height);

            InitializeComponent();
        }

        private void graph1RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _choosenGraph = this.graph1RadioButton.Checked ? 0 : 1;
        }

        private void countButton_Click(object sender, EventArgs e)
        {
            this.timeTextBox.Text = "Proszę czekać, trwa obliczanie...";
            this.timeTextBox.Refresh();
            Stopwatch st = new Stopwatch();
            st.Start();
            if (this.exactAlgorithmRadioButton.Checked)
                this.resultTextBox.Text = _accurateAlgorithm.MatchGraphs(_repository.GetGraph(0), _repository.GetGraph(1)).ToString("F4");
            else
                this.resultTextBox.Text = _approximateAlgorithm.MatchGraphs(_repository.GetGraph(0), _repository.GetGraph(1)).ToString("F4");
            st.Stop();
            this.timeTextBox.Text = st.ElapsedMilliseconds.ToString();
            this.timeTextBox.Refresh();

            MessageBox.Show("Obliczenia zostały zakończone!\nWynik: " + resultTextBox.Text + "\nCzas: " + timeTextBox.Text + " [ms] ", "Wyniki");
        }

        private void randomVerticesCountNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            this.randomEdgesCountNumericUpDown.Maximum = this.randomVerticesCountNumericUpDown.Value * (this.randomVerticesCountNumericUpDown.Value - 1);
            this.randomEdgesDensityNumericUpDown.Increment = randomVerticesCountNumericUpDown.Value > 2 ? 100 / (randomVerticesCountNumericUpDown.Value * (randomVerticesCountNumericUpDown.Value - 1)) : randomVerticesCountNumericUpDown.Value * 100;
     
            if (this.completeGraphCheckBox.Checked)
            {
                this.randomEdgesCountNumericUpDown.Value = this.randomEdgesCountNumericUpDown.Maximum;
                this.randomEdgesDensityNumericUpDown.Increment = 100;
            }
        }

        private void randomGenerateButton_Click(object sender, EventArgs e)
        {
            CreateAndDrawGraph((int)randomVerticesCountNumericUpDown.Value, (int)randomEdgesCountNumericUpDown.Value);
        }

        private void CreateAndDrawGraph(int verticesCounter, int edgesCounter)
        {
            byte[,] randomMatrix = GenerateRandomGraph(verticesCounter, edgesCounter);
            Graph randomGraph = new Graph(randomMatrix);

            _repository.SetGraph(_choosenGraph, randomGraph);
            DrawGraph(randomGraph);
        }

        private byte[,] GenerateRandomGraph(int verticesCounter, int edgesCounter)
        {
            byte[,] randomMatrix = new byte[verticesCounter, verticesCounter];

            Random rand = new Random();

            if (edgesCounter == verticesCounter * (verticesCounter - 1))
            {
                for (int i = 0; i < verticesCounter; ++i)
                {
                    for (int j = 0; j < verticesCounter; ++j)
                    {
                        if (i == j) continue;
                        randomMatrix[i, j] = 1;
                    }
                }
            }
            else
            {
                int counter = 0;
                while (counter < edgesCounter)
                {
                    int i = rand.Next(verticesCounter);
                    int j = rand.Next(verticesCounter);
                    if (randomMatrix[i, j] == 0 && i != j)
                    {
                        randomMatrix[i, j] = 1;
                        ++counter;
                    }
                }
            }

            return randomMatrix;
        }

        private void completeGraphCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.completeGraphCheckBox.Checked)
            {
                this.randomEdgesCountNumericUpDown.Value = this.randomVerticesCountNumericUpDown.Value * (this.randomVerticesCountNumericUpDown.Value - 1);
                this.randomEdgesCountNumericUpDown.Enabled = false;
                randomEdgesDensityNumericUpDown.Enabled = false;
            }
            else
            {
                this.randomEdgesCountNumericUpDown.Enabled = true;
                randomEdgesDensityNumericUpDown.Enabled = true;
            }
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _repository.SetGraph(_choosenGraph, _serializer.ReadFromFile(openFileDialog.FileName));

                string[] spliter = openFileDialog.FileName.ToString().Split('\\');

                if (0 == _choosenGraph)
                    fileName1Label.Text = "Plik: " + spliter[spliter.Length - 1];
                else
                    fileName2Label.Text = "Plik: " + spliter[spliter.Length - 1];
                ReDrawGraphs();
            }
        }

        private void saveFileButton_Click(object sender, EventArgs e)
        {
            string[] label1= graph1NameLabel.Text.Split(':');
            string[] label2 = graph2NameLabel.Text.Split(':');

            saveFileDialog.FileName = _choosenGraph == 0 ? label1[1]: label2[1];
            saveFileDialog.ShowDialog();
        }

        private void saveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveFileDialog.FileName;
            _serializer.WriteToFile(name, _repository.GetGraph(_choosenGraph));
        }

        private void manualGenerateButton_Click(object sender, EventArgs e)
        {
            CreateAndDrawGraph((int)manualVerticesCountNumericUpDown.Value, 0);
        }

        private void DrawGraph(Graph graphToDraw)
        {
            switch (_choosenGraph)
            {
                case 0:
                    pictureBox0.Image = _drawingManager.DrawGraph(graphToDraw);
                    pictureBox0.Refresh();
                    break;
                case 1:
                    pictureBox1.Image = _drawingManager.DrawGraph(graphToDraw);
                    pictureBox1.Refresh();
                    break;
            }
            ReNameGraphs();
        }

        private void AddVertexButton_Click(object sender, EventArgs e)
        {
            _repository[_choosenGraph].AddVertex();
            DrawGraph(_repository[_choosenGraph]);
        }

        private void DeleteVertexButton_Click(object sender, EventArgs e)
        {
            _repository[_choosenGraph].DeleteSelected();
            DrawGraph(_repository[_choosenGraph]);
        }

        private void manualDeleteButton_Click(object sender, EventArgs e)
        {
            CreateAndDrawGraph(0, 0);
        }

        private void pictureBox0_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxMouseUpEvent(0, new Point(e.X, e.Y));
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxMouseUpEvent(1, new Point(e.X, e.Y));
        }

        private void pictureBoxMouseUpEvent(int clickedGraph, Point p)
        {
            if (clickedGraph != _choosenGraph)
            {
                _choosenGraph = clickedGraph;
                this.graph1RadioButton.Checked = clickedGraph == 0 ? true : false;
                this.graph2RadioButton.Checked = !this.graph1RadioButton.Checked;
            }

            bool isEdge;
            _drawingManager.MouseClick(_repository[clickedGraph], new Point(p.X, p.Y), out isEdge);

            CheckIfDeleteEdgeEnable(isEdge);
            DrawGraph(_repository[clickedGraph]);

            CheckEnablingDeleteVertexButton(clickedGraph);
            CheckEnablingDeleteEdgeButton(clickedGraph);
        }

        private void CheckEnablingDeleteVertexButton(int clickedGraph)
        {
            DeleteVertexButton.Enabled = _repository[clickedGraph].HasSelectedVertex() ? true : false;
        }

        private void CheckEnablingDeleteEdgeButton(int clickedGraph)
        {
            this.DeleteEdgeButton.Enabled = _repository[clickedGraph].HasSelectedEdge();
        }

        private void CheckIfDeleteEdgeEnable(bool isEdge)
        {
            DeleteEdgeButton.Enabled = isEdge;
        }

        private void DeleteEdgeButton_Click(object sender, EventArgs e)
        {
            _repository[_choosenGraph].DeleteEdge();
            DrawGraph(_repository[_choosenGraph]);

            DeleteVertexButton.Enabled = false;
            DeleteEdgeButton.Enabled = false;
        }

        private void ReDrawGraphs()
        {
            pictureBox0.Image = _drawingManager.DrawGraph(_repository[0]);
            pictureBox0.Refresh();
            pictureBox1.Image = _drawingManager.DrawGraph(_repository[1]);
            pictureBox1.Refresh();

            ReNameGraphs();
        }

        private void ReNameGraphs()
        {
            graph1NameLabel.Text = String.Format("Nazwa:g_{0}_vertices_{1}_edges", _repository[0].NumberOfVertices, _repository[0].NumberOfEdges);
            graph2NameLabel.Text = String.Format("Nazwa:g_{0}_vertices_{1}_edges", _repository[1].NumberOfVertices, _repository[1].NumberOfEdges);
        }

        private void pictureBox0_SizeChanged(object sender, EventArgs e)
        {
            _drawingManager.Resize(pictureBox0.Size.Width, pictureBox0.Size.Height);
            ReDrawGraphs();
        }

        private void pictureBox0_MouseMove(object sender, MouseEventArgs e)
        {
            this.mousePointTextBox0.Text = String.Format("{0};{1}", e.X, e.Y);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.mousePointTextBox1.Text = String.Format("{0};{1}", e.X, e.Y);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            _drawingManager.SetMaximumDrawingVertices((int)this.numberOfDrawingVerticesNumericUpDown.Value);
            ReDrawGraphs();
        }

        private void pictureBox0_DoubleClick(object sender, EventArgs e)
        {
            pictureBoxDoubleClickEvent(0);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            pictureBoxDoubleClickEvent(1);
        }

        private void pictureBoxDoubleClickEvent(int clickedPicture)
        {
            _repository[clickedPicture].DeselectAll();
            ReDrawGraphs();
        }

        private void ComplementaryGraphButton_Click(object sender, EventArgs e)
        {
            _repository[_choosenGraph].ChangeToComplementary();
            DrawGraph(_repository[_choosenGraph]);
        }

        private void renumberButton_Click(object sender, EventArgs e)
        {
            _repository[_choosenGraph].Renumber();
            ReDrawGraphs();
        }

        private void randomEdgesCountNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            this.randomEdgesDensityNumericUpDown.ValueChanged -= new System.EventHandler(this.randomEdgesDensityNumericUpDown_ValueChanged);
            randomEdgesDensityNumericUpDown.Value = 100 * randomEdgesCountNumericUpDown.Value / (randomVerticesCountNumericUpDown.Value * (randomVerticesCountNumericUpDown.Value - 1));
            this.randomEdgesDensityNumericUpDown.ValueChanged += new System.EventHandler(this.randomEdgesDensityNumericUpDown_ValueChanged);
        }

        private void randomEdgesDensityNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            this.randomEdgesCountNumericUpDown.ValueChanged -= new System.EventHandler(this.randomEdgesCountNumericUpDown_ValueChanged);
            randomEdgesCountNumericUpDown.Value = (int)(randomEdgesDensityNumericUpDown.Value * randomVerticesCountNumericUpDown.Value * (randomVerticesCountNumericUpDown.Value - 1))/100;
            this.randomEdgesCountNumericUpDown.ValueChanged += new System.EventHandler(this.randomEdgesCountNumericUpDown_ValueChanged);
        } 
    }
}
