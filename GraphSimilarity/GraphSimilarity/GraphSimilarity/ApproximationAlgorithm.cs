﻿using GraphSimilarity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accurateGraphMatchingAlgorithm
{
    class ApproximationAlgorithm : GraphMatchAlgorithm
    {
        public struct Pair
        {
            public int v;
            public int d;
        }

        //Tworzy grafy proste na podstawie grafu skierowanego, zgodnie z dokumentacją
        public void CreateSimGraphs(Graph G, out Graph a, out Graph b)
        {
            a = new Graph(G.NumberOfVertices);
            b = new Graph(G.NumberOfVertices);

            for (int i = 0; i < G.NumberOfVertices; i++)
            {
                for (int j = 0; j < G.NumberOfVertices; j++)
                {
                    if (G.AdjacencyMatrix[i, j] == 1 && G.AdjacencyMatrix[j, i] == 1)
                    {
                        a.AdjacencyMatrix[i, j] = 1;
                        a.AdjacencyMatrix[j, i] = 1;
                    }

                    if (G.AdjacencyMatrix[i, j] == 1 || G.AdjacencyMatrix[j, i] == 1)
                    {
                        b.AdjacencyMatrix[i, j] = 1;
                        b.AdjacencyMatrix[j, i] = 1;
                    }
                }
            }
        }


        //Tworzy graf będący produktem dwóch grafów
        public IGraph CreateProduct(Graph a, Graph b, out List<Pair> L)
        {
            // FGraph c = new FGraph(a.NumberOfVertices * b.NumberOfVertices);
            int n = a.NumberOfVertices;
            int cn = a.NumberOfVertices * b.NumberOfVertices;
            List<Pair> L2 = new List<Pair>(cn);

            for (int i = 0; i < cn; i++)
            {
                Pair p;
                p.v = i;
                p.d = 0;
                L2.Add(p);
            }

            Parallel.For(0, cn, (i) =>
            {
                Pair p = L2[i];

                int ai = i % n;
                int bi = i / n;

                int aj = 0;
                int bj = 0;
                int qq = b[bi, bj];
                bool qq1 = qq == 1;
                bool qqn1 = qq != 1;

                for (int j = 0; j < i; j++)
                {
                    int pp = a[ai, aj];

                    if ((pp == 1 && qq1) ||
                        (pp != 1 && qqn1))
                    //if (((pp & qq) | (1 ^ (pp | qq))) == 1)
                    {
                        //c[i, j] = 1;
                        p.d++;
                    }

                    aj++;
                    if (aj >= n)
                    {
                        bj++;
                        if (bj >= n) bj = 0;
                        qq = b[bi, bj];
                        qq1 = qq == 1;
                        qqn1 = qq != 1;

                        aj = 0;
                    }
                }
                L2[i] = p;
            });
            L = L2;
            L.Sort((x, y) => y.d.CompareTo(x.d));
            return null;
        }

        //Tworzy listę wierzchoków posortowaną wedłuch ich stopnia
        public List<Pair> CreateSortedList(IGraph a)
        {
            List<Pair> L = new List<Pair>(a.NumberOfVertices);

            for (int i = 0; i < a.NumberOfVertices; i++)
            {
                Pair p;
                p.v = i;
                p.d = 0;
                L.Add(p);
            }

            Parallel.For(0, a.NumberOfVertices, (i) =>
            {
                Pair p = L[i];
                for (int j = 0; j < a.NumberOfVertices; j++)
                {
                    if (a[i, j] == 1)
                    {
                        p.d++;
                    }
                }
                L[i] = p;
            });
            //sortowanie quicsort, argumentem jest funkcja porównująca (komparator)
            L.Sort((x, y) => y.d.CompareTo(x.d));
            return L;
        }


        // Zachłanny algorymt poszukiwania kliki
        // W oparciu o posortowaną wg, stopni listę wierzchołków
        public List<int> FindQlique(List<Pair> L, IGraph A, IGraph B)
        {
            int n = A.NumberOfVertices;
            List<int> Q = new List<int>(L.Count);
            if (L.Count == 0)
                return Q;
            Q.Add(L[0].v);
            L.RemoveAt(0);

            foreach (Pair p in L)
            {
                bool ok = true;

                for (int i = 0; i < Q.Count; i++)
                {
                    int ai = p.v % n;
                    int bi = p.v / n;
                    int aj = Q[i] % n;
                    int bj = Q[i] / n;

                    int pp = A[ai, aj];
                    int qq = B[bi, bj];

                    if ((pp == 1 && qq == 1) || (pp != 1 && qq != 1))
                    {
                        ok = false;
                        break;
                    }
                }
                if (ok)
                    Q.Add(p.v);
            }

            return Q;
        }

        // Ustala odpowiedniki wierzchołków z listy Q w grafie G
        // Lista Q zawiera wierzchołki należące do produktu grafu G z pewnym grafem G2
        public void GetOrgV(List<int> Q, Graph G, Graph H, out List<int> V1, out List<int> V2)
        {
            int n = G.NumberOfVertices;
            V1 = new List<int>(Q.Count);
            V2 = new List<int>(Q.Count);

            foreach (int i in Q)
            {
                int v = i % n;
                bool find = false;
                foreach (int j in V1)
                {
                    if (j == v)
                        find = true;
                }

                if (!find)
                    V1.Add(v);


                v = i / n;
                find = false;
                foreach (int j in V2)
                {
                    if (j == v)
                        find = true;
                }

                if (!find)
                    V2.Add(v);
            }
        }

        // Oblicza miarę grafu, dla grafu prostego G
        public int CalcMeasure(List<int> L1, List<int> L2, Graph G1, Graph G2)
        {
            int m = 0;

            int n = L1.Count;

            Parallel.For(0, n, (i) =>
            {
                int mm = 0;
                for (int j = 0; j < n; j++)
                {
                    if (i == j) continue;

                    int va0 = L1[i];
                    int va1 = L1[j];

                    int vb0 = L1[i];
                    int vb1 = L1[j];

                    if (G1.AdjacencyMatrix[va0, va1] == 1 && G2.AdjacencyMatrix[vb0, vb1] == 1)
                        mm++;
                }
                System.Threading.Interlocked.Add(ref m, mm);
            });


            return m + G1.NumberOfVertices;
        }


        public double Calculate(Graph A0, Graph B0)
        {
            //Tworzymy produkcty grafów uproszczonych
            List<Pair> L0;
            CreateProduct(A0, B0, out L0);

            //Tworzymy listy posortowanych wg. stopnia wierzchołków z produktów P0, P1
            //Console.Error.WriteLine("Sort");
            //List<Pair> L0 = CreateSortedList(P0);

            //Szukamy kliki w produktach P0, P1
            List<int> Q0 = FindQlique(L0, A0, B0);

            //Ustalamy które wierzchołki z grafów A0, A1 wchodzą w skład znalezionych klik
            List<int> V0A, V0B;
            GetOrgV(Q0, A0, B0, out V0A, out V0B);


            //Obliczamy miarę dla grafów uproszczonych
            double m0 = CalcMeasure(V0A, V0B, A0, B0);

            //Obliczamy podobieństwo
            double p0 = m0 / (A0.NumberOfVertices + A0.NumberOfEdges + B0.NumberOfVertices + B0.NumberOfEdges - m0);

            return p0;
        }

        //Główna metoda obliczająca podobieństwo grafów
        public double MatchGraphs(Graph g1, Graph g2)
        {
            Console.Error.WriteLine("Start");
            //Obsługa przypadku szczególnego
            if (0 == g1.NumberOfVertices)
                if (0 == g2.NumberOfVertices)
                    return 1;
                else
                    return 0;

            if (g1.NumberOfVertices > g2.NumberOfVertices)
            {
                Graph tmp = g2;
                g2 = new Graph(g1);
                g1 = new Graph(tmp);
            }

            //Tworzymy grafu uproszczone
            Graph A0, A1, B0, B1;
            CreateSimGraphs(g1, out A0, out A1);
            CreateSimGraphs(g2, out B0, out B1);
            A0.Calc();
            A1.Calc();
            B0.Calc();
            B1.Calc();



            double p0 = Calculate(A0, B0);
            double p1 = Calculate(A1, B1);

            //Zwracamy średnią z dwóch uzyskanych podobieństw
            return 0.5 * (p1 + p0);
        }
    }
}
