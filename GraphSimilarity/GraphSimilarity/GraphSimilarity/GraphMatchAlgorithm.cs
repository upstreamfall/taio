﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphSimilarity
{
    interface GraphMatchAlgorithm
    {
        double MatchGraphs(Graph g1, Graph g2);
    }
}
