﻿using System;
using System.Drawing;
using System.Linq;

namespace GraphSimilarity
{

    interface IGraph
    {
        int NumberOfVertices { get; set; }
        int NumberOfEdges { get; set; }
        byte this[int a, int b] { get; set; }
    }

    class Graph : IGraph
    {
        public int NumberOfVertices {get; set;}
        public int NumberOfEdges {get; set;}
        public byte[,] AdjacencyMatrix {get; set;}

        public int SelectedVertex { get; set; }
        public int SelectedVertex2 { get; set; }

        public Graph(byte[,] _adjacencyMatrix)
        {
            SelectedVertex = -1;
            SelectedVertex2 = -1;
            NumberOfVertices = _adjacencyMatrix.GetLength(0);
            AdjacencyMatrix = new byte[NumberOfVertices, NumberOfVertices];
            for (int i = 0; i < NumberOfVertices; i++)
            {
                for (int j = 0; j < NumberOfVertices; j++)
                {
                    AdjacencyMatrix[i, j] = _adjacencyMatrix[i, j];
                    if (0 != AdjacencyMatrix[i, j])
                    {
                        NumberOfEdges++;
                    }
                }
            }
        }

        public Graph(Graph newGraph)
        {
            SelectedVertex = -1;
            SelectedVertex2 = -1;
            this.NumberOfVertices = newGraph.NumberOfVertices;
            this.NumberOfEdges = newGraph.NumberOfEdges;
            this.AdjacencyMatrix = (byte[,])newGraph.AdjacencyMatrix.Clone();
        }

        public Graph(int NumberOfVertices=0)
        {
            SelectedVertex = -1;
            SelectedVertex2 = -1;
            this.NumberOfVertices = NumberOfVertices;
            NumberOfEdges = 0;
            AdjacencyMatrix = new byte[NumberOfVertices, NumberOfVertices];
        }

        public byte this[int first, int second]
        {
            get
            {
                return AdjacencyMatrix[first, second];
            }
            set
            {
                AdjacencyMatrix[first, second] = value;
            }
        }

        public override string ToString()
        {
            string fileFormat = NumberOfVertices.ToString();
            
            for (int i = 0; i<NumberOfVertices; i++)
            {
                fileFormat +='\n';
                for (int j = 0; j < NumberOfVertices; ++j)
                {
                    fileFormat += AdjacencyMatrix[i, j].ToString();
                }
            }
            return fileFormat;
        }

        internal bool HasSelectedVertex()
        {
            return SelectedVertex > -1 || SelectedVertex2 > -1;
        }

        internal void DeleteSelected()
        {            
            DeleteVertexByIndex(SelectedVertex);
        
               if( SelectedVertex2 > -1){
                   if (SelectedVertex2 > SelectedVertex)
                       --SelectedVertex2;
                   DeleteVertexByIndex(SelectedVertex2);
               }
            
            SelectedVertex = -1;
            SelectedVertex2 = -1;
        }

        private void DeleteVertexByIndex(int verticleToDelete)
        {
            byte[,] newMatrix = new byte[NumberOfVertices - 1, NumberOfVertices - 1];
            int newNumberOfEdges = 0;
            int afterSelected = 0;

            for (int i = 0; i < verticleToDelete; ++i)
            {
                afterSelected = 0;
                for (int j = 0; j < NumberOfVertices; ++j)
                {
                    if (verticleToDelete == j)
                    {
                        afterSelected = 1;
                        continue;
                    }
                    if (1 == AdjacencyMatrix[i, j])
                        ++newNumberOfEdges;
                    newMatrix[i, j - afterSelected] = AdjacencyMatrix[i, j];
                }
            }
            for (int i = verticleToDelete + 1; i < NumberOfVertices; ++i)
            {
                afterSelected = 0;
                for (int j = 0; j < NumberOfVertices; ++j)
                {
                    if (verticleToDelete == j)
                    {
                        afterSelected = 1;
                        continue;
                    }
                    if (1 == AdjacencyMatrix[i, j])
                        ++newNumberOfEdges;
                    newMatrix[i - 1, j - afterSelected] = AdjacencyMatrix[i, j];
                }
            }

            --NumberOfVertices;
            NumberOfEdges = newNumberOfEdges;
            AdjacencyMatrix = (byte[,])newMatrix.Clone();
        }

        internal void AddVertex()
        {
            byte[,] newMatrix = new byte[NumberOfVertices + 1, NumberOfVertices + 1];

            for (int i = 0; i < NumberOfVertices; ++i)
                for (int j = 0; j < NumberOfVertices; ++j)
                {
                    newMatrix[i, j] = AdjacencyMatrix[i, j];
                }

            ++NumberOfVertices;
            AdjacencyMatrix = (byte[,])newMatrix.Clone();
        }

        internal bool AddNotExistsEdge(int secondVertex)
        {
            int firstVertex = SelectedVertex > -1 ? SelectedVertex : SelectedVertex2;

            if (IsEdge(firstVertex, secondVertex))
                return false;

            AddEdge(firstVertex, secondVertex);
            return true;
        }

        private bool IsEdge(int firstVertex, int secondVertex)
        {
            return 1 == AdjacencyMatrix[firstVertex, secondVertex];
        }

        private void AddEdge(int firstVertex, int secondVertex)
        {
            if (1 == AdjacencyMatrix[firstVertex, secondVertex])
                return;

            AdjacencyMatrix[firstVertex, secondVertex] = 1;
            ++NumberOfEdges;

            if (-1 == SelectedVertex)
            {
                SelectedVertex = SelectedVertex2;
                SelectedVertex2 = secondVertex;
            }
            else
                SelectedVertex2 = secondVertex;

            //SelectedVertex = -1;
            //SelectedVertex2 = -1;
        }

        internal void DeleteEdge()
        {
            AdjacencyMatrix[SelectedVertex, SelectedVertex2] = 0;
            --NumberOfEdges;
            //Deselect(SelectedVertex);
            DeselectAll();
        }

        internal void SelectEdge(int secondVertex)
        {
            SelectedVertex = SelectedVertex == -1 ? secondVertex : SelectedVertex;
            SelectedVertex2 = SelectedVertex2 == -1 ? secondVertex : SelectedVertex2;
        }

        internal void Deselect(int vertex)
        {
            //SelectedVertex = -1;
            //SelectedVertex2 = -1;

            if (SelectedVertex == vertex)
                SelectedVertex = -1;
            if (SelectedVertex2 == vertex)
                SelectedVertex2 = -1;
        }

        internal bool IsSelectedEdge(int first, int second)
        {
            return SelectedVertex == first && SelectedVertex2 == second;
        }

        public void Calc()
        {
            NumberOfEdges = 0;
            for (int i = 0; i < NumberOfVertices; i++)
            {
                for (int j = 0; j < NumberOfVertices; j++)
                {
                    if (AdjacencyMatrix[i, j] != 0)
                    {
                        NumberOfEdges++;
                    }
                }
            }
        }
        
        public void Print()
        {
            for (int i = 0; i < NumberOfVertices; i++)
            {
                for (int j = 0; j < NumberOfVertices; j++)
                {
                    Console.Write(AdjacencyMatrix[i, j]);
                }
                Console.WriteLine();
            }
        }

        internal bool HasSelectedEdge()
        {
            return SelectedVertex > -1 && SelectedVertex2 > -1;
        }

        internal void DeselectAll()
        {
            SelectedVertex = SelectedVertex2 = -1;
        }

        internal void ChangeToComplementary()
        {
            int edges = 0;
            for (int i = 0; i < NumberOfVertices; ++i)
                for (int j = 0; j < NumberOfVertices; ++j)
                {
                    if (i == j)
                        continue;
                    if (AdjacencyMatrix[i, j] == 0)
                    {
                        AdjacencyMatrix[i, j] = 1;
                        edges++;
                    }
                    else
                        AdjacencyMatrix[i, j] = 0;
                }
            NumberOfEdges = edges;
        }

        internal void Renumber()
        {
            var random = new Random();
            int[] intArray = Enumerable.Range(0, NumberOfVertices).OrderBy(t => random.Next()).ToArray();

            byte[,] newAdjacencyMatrix = new byte[NumberOfVertices, NumberOfVertices];
            for (int i = 0; i < NumberOfVertices; ++i)
                for (int j = 0; j < NumberOfVertices; j++)
                    newAdjacencyMatrix[intArray[i], intArray[j]] = AdjacencyMatrix[i, j];
            AdjacencyMatrix = (byte[,])newAdjacencyMatrix.Clone();
        }
    }







    class FGraph : IGraph
    {
        public int NumberOfVertices { get; set; }
        public int NumberOfEdges { get; set; }
        public byte[][] AdjacencyMatrix { get; set; }
        public UInt64[] Lut;

        public FGraph(int NumberOfVertices = 0)
        {
            this.NumberOfVertices = NumberOfVertices;
            NumberOfEdges = 0;

            UInt64 sum = 0;
            Lut = new UInt64[NumberOfVertices+1];
            for (int i = 0; i <= NumberOfVertices; i++)
            {
                sum += (UInt64)i;
                Lut[i] = sum;
            }
            
            Int64 size  = (Int64)(sum + 8) / 8;

            if (size > 1<<30)
            {
                int msize = (int)(size >> 30 + 1);
                AdjacencyMatrix = new byte[msize][];
                for (int i = 0; size > 0; size -= (1 << 30), i++)
                {
                    if (size > 1 << 30)
                        AdjacencyMatrix[i] = new byte[(1<<30)];
                    else
                        AdjacencyMatrix[i] = new byte[size];
                }
                
            }else{
                AdjacencyMatrix = new byte[1][];
                AdjacencyMatrix[0] = new byte[size];
            }
        }

        public byte this[int f, int s]
        {
            get
            {
                if (s > f)
                {
                    int tmp = s;
                    s = f;
                    f = tmp;
                }

                UInt64 base_addr = Lut[f];
                UInt64 base_tot_addr = base_addr + (UInt64)s;
                UInt64 addr = base_tot_addr / 8;
                UInt64 x = addr >> 30;
                UInt64 y = addr & ~((~(UInt64)0) << 29);

                int div = (int)(base_tot_addr % 8);

                byte val = AdjacencyMatrix[x][y];
                int sh = 7 - div;
                byte rv = (byte)((val >> sh) & 1);
                return rv;
            }
            set
            {
                if (s > f)
                {
                    int tmp = s;
                    s = f;
                    f = tmp;
                }

                UInt64 base_addr = Lut[f];
                UInt64 base_tot_addr = base_addr + (UInt64)s;
                UInt64 addr = base_tot_addr / 8;
                UInt64 x = addr >> 30;
                UInt64 y = addr & ~((~(UInt64)0) << 29);

                int div = (int)(base_tot_addr % 8);

                int val = AdjacencyMatrix[x][y];
                int sh = 7 - div;

                if (value == 0)
                {
                    val &= ~(1 << sh);
                    AdjacencyMatrix[x][y] = (byte)val;
                }
                else
                {
                    val |= (1 << sh);
                    AdjacencyMatrix[x][y] = (byte)val;
                }
            }
        }

        public void Calc()
        {
            NumberOfEdges = 0;
            for (int i = 0; i < NumberOfVertices; i++)
            {
                for (int j = 0; j < NumberOfVertices; j++)
                {
                    if ((this)[i, j] != 0)
                    {
                        NumberOfEdges++;
                    }
                }
            }
        }
    }
}
