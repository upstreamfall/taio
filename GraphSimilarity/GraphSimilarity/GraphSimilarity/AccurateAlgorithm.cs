﻿using System;
using System.Windows.Forms;

namespace GraphSimilarity
{
    class AccurateAlgorithm : GraphMatchAlgorithm
    {
        public double MatchGraphs(Graph g1, Graph g2)
        {
            try
            {
                double matchVal;
                //jeśli oba grafy są puste, zwracamy 1
                if (g1.NumberOfVertices == 0 && g2.NumberOfVertices == 0)
                {
                    matchVal = 1;
                    return matchVal;
                }
                //jeśli jeden z grafów jest pusty, zwracamy 0
                if (g1.NumberOfVertices == 0 || g2.NumberOfVertices == 0)
                {
                    matchVal = 0;
                    return matchVal;
                }
                //jeśli g1 ma więcej wierzchołków niż g2, zamieniamy je
                if (g1.NumberOfVertices > g2.NumberOfVertices)
                {
                    Graph tmp = new Graph(g2.AdjacencyMatrix);
                    g2 = new Graph(g1.AdjacencyMatrix);
                    g1 = new Graph(tmp.AdjacencyMatrix);
                }

                int v1 = g1.NumberOfVertices;
                int v2 = g2.NumberOfVertices;
                int e1 = g1.NumberOfEdges;
                int e2 = g2.NumberOfEdges;

                //numerujemy krawędzie grafu 1
                int[,] edgesG1 = new int[e1, 2];
                int l = 0;
                for (int i = 0; i < v1; i++)
                {
                    for (int j = 0; j < v1; j++)
                    {
                        if (g1.AdjacencyMatrix[i, j] != 0)
                        {
                            edgesG1[l, 0] = i;
                            edgesG1[l, 1] = j;
                            l++;
                        }
                    }
                }

                //numerujemy krawędzie grafu 2
                int[,] edgesG2 = new int[e2, 2];
                l = 0;
                for (int i = 0; i < v2; i++)
                {
                    for (int j = 0; j < v2; j++)
                    {
                        if (g2.AdjacencyMatrix[i, j] != 0)
                        {
                            edgesG2[l, 0] = i;
                            edgesG2[l, 1] = j;
                            l++;
                        }
                    }
                }

                //tworzymy macierze D[0]..D[p1] oraz D i maxD
                int[][,] D = new int[v1 + 1][,];
                for (int i = 0; i < D.Length; i++)
                {
                    D[i] = new int[e1, e2];
                    for (int j = 0; j < e1; j++)
                    {
                        for (int k = 0; k < e2; k++)
                        {
                            D[i][j, k] = 1;
                        }
                    }
                }
                int[,] tmpD, maxD;
                tmpD = new int[e1, e2];
                maxD = new int[e1, e2];
                for (int i = 0; i < e1; i++)
                {
                    for (int j = 0; j < e2; j++)
                    {
                        tmpD[i, j] = 1;
                        maxD[i, j] = 1;
                    }
                }

                //tworzymy macierze zajete, sprawdzone, sparowanie
                int[] used = new int[v2];
                int[,] tried = new int[v1, v2];
                int[] match = new int[v1];

                //tworzymy zmienne x[1]..x[p1]
                int[] x = new int[v1];

                //tworzymy zmienne liczbowe krawedzie i maxKrawedzie
                int tmpEdges = e1;
                int maxEdges = 0;

                int index = 1;

                for (int i = 0; i < v2; i++)
                {
                    used[i] = 0;
                    tried[index - 1, i] = 0;
                }

                //główna pętla
                do
                {
                    //sprawdzamy, czy są jakieś niezajęte i niesprawdzone wierzchołki w G2, które można przypisać do i-tego wierzchołka G1
                    bool unusedAndUntriedExists = false;
                    int unusedAndUntriedIndex = -1;
                    for (int i = 0; i < v2; i++)
                    {
                        if (used[i] == 0 && tried[index - 1, i] == 0)
                        {
                            unusedAndUntriedExists = true;
                            unusedAndUntriedIndex = i;
                            break;
                        }
                    }
                    //jeśli są:
                    if (unusedAndUntriedExists)
                    {
                        //przypisujemy pierwszy nieużywany i niesprawdzony
                        x[index - 1] = unusedAndUntriedIndex;
                        tried[index - 1, x[index - 1]] = 1;
                        used[x[index - 1]] = 1;
                        //obliczmy aktualny stan macierzy D
                        for (int i = 0; i < e1; i++)
                        {
                            for (int j = 0; j < e2; j++)
                            {
                                if ((tmpD[i, j] == 1) && ((edgesG1[i, 0] == index - 1 && edgesG2[j, 0] != x[index - 1]) || (edgesG1[i, 1] == index - 1 && edgesG2[j, 1] != x[index - 1])))
                                {
                                    tmpD[i, j] = 0;
                                }
                            }
                        }
                        //obliczamy aktualny stan zmiennej krawedzie
                        tmpEdges = 0;
                        for (int i = 0; i < e1; i++)
                        {
                            for (int j = 0; j < e2; j++)
                            {
                                if (tmpD[i, j] == 1)
                                {
                                    tmpEdges++;
                                    break;
                                }
                            }
                        }
                        //jeśli krawedzie > maxKrawedzie
                        if (tmpEdges > maxEdges)
                        {
                            //jeśli przypisaliśmy już wierzchołki wszystkim wierzchołkom z G1
                            if (index == v1)
                            {
                                //zapisujemy aktualny stan jako najbardziej optymalny
                                for (int i = 0; i < v1; i++)
                                {
                                    match[i] = x[i];
                                }
                                for (int i = 0; i < e1; i++)
                                {
                                    for (int j = 0; j < e2; j++)
                                    {
                                        maxD[i, j] = tmpD[i, j];
                                    }
                                }
                                maxEdges = tmpEdges;
                                for (int i = 0; i < e1; i++)
                                {
                                    for (int j = 0; j < e2; j++)
                                    {
                                        tmpD[i, j] = D[index - 1][i, j];
                                    }
                                }
                                used[x[index - 1]] = 0;
                            }
                            //jeszcze nie wszystkie wierzchołki z G1 mają sparowanie
                            else
                            {
                                //będziemy przypisywać kolejny wierzchołek
                                for (int i = 0; i < e1; i++)
                                {
                                    for (int j = 0; j < e2; j++)
                                    {
                                        D[index][i, j] = tmpD[i, j];
                                    }
                                }
                                index++;
                                for (int i = 0; i < v2; i++)
                                {
                                    tried[index - 1, i] = 0;
                                }
                            }
                        }
                        //aktualna liczba w zmiennej krawedzie nie prowadzi do lepszego rozwiązania
                        else
                        {
                            for (int i = 0; i < e1; i++)
                            {
                                for (int j = 0; j < e2; j++)
                                {
                                    tmpD[i, j] = D[index - 1][i, j];
                                }
                            }
                            used[x[index - 1]] = 0;
                        }
                    }
                    //nie ma już wolnych i niesprawdzonych wierzchołków w G2 - cofamy się o poziom wyżej
                    else
                    {
                        index--;
                        if (index > 0)
                        {
                            for (int i = 0; i < e1; i++)
                            {
                                for (int j = 0; j < e2; j++)
                                {
                                    tmpD[i, j] = D[index - 1][i, j];
                                }
                            }
                            used[x[index - 1]] = 0;
                        }
                    }
                }
                while (index != 0);

                //obliczamy współczynnik podobieństwa
                matchVal = (double)(v1 + maxEdges) / (double)(e1 + v2 + e2 - maxEdges);
                matchVal = Math.Round(matchVal, 4);

                //komunikaty testowe
                Console.WriteLine("MaxEdges: {0}", maxEdges);
                Console.WriteLine("Przyporzadkowanie wierzcholkow (G1 - graf o mniejszej liczbie wierzcholkow):");
                Console.Write("G1: ");
                for (int i = 0; i < v1; i++)
                {
                    Console.Write("{0} ", i);
                }
                Console.WriteLine();
                Console.Write("G2: ");
                for (int i = 0; i < v1; i++)
                {
                    Console.Write("{0} ", match[i]);
                }
                Console.WriteLine();

                return matchVal;
            }
            catch (OutOfMemoryException)
            {
                MessageBox.Show("Brak pamięci!");
                return 0;
            }
            
        }
    }
}
