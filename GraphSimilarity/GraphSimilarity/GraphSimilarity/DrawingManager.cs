﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphSimilarity
{
    class DrawingManager
    {
        private int _width, _height;
        private Bitmap Bitmaps {get; set;}

        private int LimitOfVertices = 20;
        private const int PointSize = 30;
        private const int PointCorrection = PointSize / 2;
        private int Radius = 200;
        private const int StartingAngle = 90;
        private Point Center;

        private Pen PenForGraph = new Pen(Color.Red, 8);
        private Pen PenForSelectedEdge = new Pen(Color.Green, 8);
        private SolidBrush BrushToDraw = new SolidBrush(Color.Blue);

        public DrawingManager(int _width, int _height)
        {
            this._width = _width;
            this._height = _height;
            Center = new Point(_width / 2, _height / 2);

            PenForGraph.EndCap = LineCap.ArrowAnchor;
            PenForGraph.StartCap = LineCap.NoAnchor;

            PenForSelectedEdge.StartCap = LineCap.RoundAnchor;
            PenForSelectedEdge.EndCap = LineCap.ArrowAnchor;
        }

        private Point DegreesToXY(float degrees, float radius, Point origin)
        {
            Point xy = new Point();
            double radians = degrees * Math.PI / 180.0;

            xy.X = (int)(Math.Cos(radians) * radius + origin.X);
            xy.Y = (int)(Math.Sin(-radians) * radius + origin.Y);

            return xy;
        }

        private Point[] CalculateVertices(int numberOfVertices, int radius, int startingAngle, Point center)
        {
            List<Point> points = new List<Point>();
            float step = 360.0f / numberOfVertices;

            float angle = startingAngle; //starting angle
            for (double i = startingAngle; i < startingAngle + 360.0; i += step) //go in a full circle
            {
                points.Add(DegreesToXY(angle, radius, center)); //code snippet from above
                angle += step;
            }

            return points.ToArray();
        }

        internal Image DrawGraph(Graph graph)
        {
            int verticesCounter = graph.NumberOfVertices;
            Bitmap bitmap = new Bitmap(_width, _height);

            if (0 == verticesCounter || LimitOfVertices < verticesCounter)
                return bitmap;

            Point[] vertices = CalculateVertices(verticesCounter, Radius, StartingAngle, new Point(_width / 2, _height / 2));

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                //drawing edges
                for (int i = 0; i < verticesCounter; ++i)
                    for (int j = 0; j < verticesCounter; ++j)
                        if (1 == graph[i, j])
                        {
                            Point p1 = new Point(vertices[i].X , vertices[i].Y);
                            Point p2 = new Point(vertices[j].X + (Math.Abs(vertices[i].Y - vertices[j].Y) < 2 ? (vertices[i].X < vertices[j].X ? -PointCorrection : PointCorrection) : 0),
                                                 vertices[j].Y + (Math.Abs(vertices[i].Y - vertices[j].Y) < 2 ? 0 : (vertices[i].Y < vertices[j].Y ? -PointCorrection : PointCorrection)));

                            if (graph.IsSelectedEdge(i, j))
                                graphics.DrawLine(PenForSelectedEdge, p1, p2);
                            else
                            graphics.DrawLine(PenForGraph, p1, p2);
                        }

                //drawing points
                for (int i = 0; i < verticesCounter; ++i)
                    graphics.FillEllipse(BrushToDraw, vertices[i].X - PointCorrection, vertices[i].Y - PointCorrection, PointSize, PointSize);

                //draw border for selected verticle if exist
                if (graph.HasSelectedVertex())
                {
                    if (graph.HasSelectedVertex())
                    {
                        if (-1 < graph.SelectedVertex)
                        {
                            graphics.DrawRectangle(new Pen(Color.Gray, 4), vertices[graph.SelectedVertex].X - PointCorrection, vertices[graph.SelectedVertex].Y - PointCorrection, PointSize, PointSize);
                        }
                        if (-1 < graph.SelectedVertex2)
                        {
                            graphics.DrawRectangle(new Pen(Color.Gray, 4), vertices[graph.SelectedVertex2].X - PointCorrection, vertices[graph.SelectedVertex2].Y - PointCorrection, PointSize, PointSize);
                        }
                        if(graph.HasSelectedEdge())
                        {
                            //draw selected edge
                            Point p1 = new Point(vertices[graph.SelectedVertex].X, vertices[graph.SelectedVertex].Y);
                            Point p2 = new Point(vertices[graph.SelectedVertex2].X + (Math.Abs(vertices[graph.SelectedVertex].Y - vertices[graph.SelectedVertex2].Y) < 2 ? (vertices[graph.SelectedVertex].X < vertices[graph.SelectedVertex2].X ? -PointCorrection : PointCorrection) : 0),
                                                 vertices[graph.SelectedVertex2].Y + (Math.Abs(vertices[graph.SelectedVertex].Y - vertices[graph.SelectedVertex2].Y) < 2 ? 0 : (vertices[graph.SelectedVertex].Y < vertices[graph.SelectedVertex2].Y ? -PointCorrection : PointCorrection)));
                            graphics.DrawLine(PenForSelectedEdge, p1, p2);
                        }
                    }
                }
            }

            return bitmap;
        }

        internal void MouseClick(Graph graph, Point click, out bool isEdge)
        {
            isEdge = false;
            int clickedVertex = SelectVertex(graph, click);
            if (-1 == clickedVertex)
                return;

            if (graph.HasSelectedVertex())
            {
                if (graph.HasSelectedEdge())
                {
                    graph.DeselectAll();
                    return;
                }
                if (clickedVertex == graph.SelectedVertex)
                {
                    graph.Deselect(clickedVertex);
                    return;
                }
                if (clickedVertex == graph.SelectedVertex2)
                {
                    graph.Deselect(clickedVertex);
                    return;
                }

                if (graph.AddNotExistsEdge(clickedVertex))
                    return;

                graph.SelectEdge(clickedVertex);
                isEdge = true;
            }
            else
            {
                graph.SelectedVertex = clickedVertex;
            }
        }

        private int SelectVertex(Graph graph, Point click)
        {
            Point[] points = CalculateVertices(graph.NumberOfVertices, Radius, StartingAngle, Center);

            for (int i = 0; i < points.Length; ++i)
            {
                double dist = Distance(click, points[i]);
                if (Distance(click, points[i]) < PointSize)
                    return i;
            }
            return -1;
        }

        private double Distance(Point point1, Point point2)
        {
            return Math.Sqrt((point1.X - point2.X) * (point1.X - point2.X) + (point1.Y - point2.Y) * (point1.Y - point2.Y));
        }

        internal void Resize(int newWeight, int newHeight)
        {
            _width = newWeight;
            _height = newHeight;
            Center = new Point(_width / 2, _height / 2);
            Radius = (int)(Math.Min(_width, _height) * 3 / 8);
        }

        internal void SetMaximumDrawingVertices(int newLimitOfVertices)
        {
            LimitOfVertices = newLimitOfVertices;
        }
    }
}
